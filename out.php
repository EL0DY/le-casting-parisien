<?php
function entete($title) {
    ?>
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title ?></title>

        <!-- Stylesheets -->

        <!-- Default stylesheets-->
        <link href="assets/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Template specific stylesheets-->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="assets/lib/animate.css/animate.css" rel="stylesheet">
        <link href="assets/lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/lib/et-line-font/et-line-font.css" rel="stylesheet">
        <link href="assets/lib/flexslider/flexslider.css" rel="stylesheet">
        <link href="assets/lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <link href="assets/lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
        <link href="assets/lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Montserrat:wght@300&display=swap" rel="stylesheet">
        

        <!-- Main stylesheet and color file-->
        <link href="assets/css/style.css" rel="stylesheet">
        <link id="color-scheme" href="assets/css/colors/default.css" rel="stylesheet">
    </head>

    <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
        <main>
        <div class="page-loader">
            <div class="loader">Loading...</div>
        </div>
    <?php
}

function pied($follow) {
    ?>
    <!-- Footer-->
    <div style="width: 100%;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2626.62922370957!2d2.3544544160148853!3d48.82713547928447!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e6718f74d45045%3A0x1fa9ae6069564e7c!2s64%20Avenue%20d&#39;Italie%2C%2075013%20Paris!5e0!3m2!1sfr!2sfr!4v1594991218825!5m2!1sfr!2sfr" height="450" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0" style="width: 100%;"></iframe>
    </div>
    <div class="module-small bg-dark" id="contact" style="background: #000;">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="widget">
                        <h5 class="widget-title font-alt">Le Casting Parisien</h5>
                        <p style="margin: 0; line-height: 1.8;">+33 (0)1 45 89 41 67</p>
                        <p style="margin: 0; line-height: 1.8;">64 avenue d’italie</p>
                        <p style="margin: 0; line-height: 1.8;">75013 Paris – France</p>
                        <p style="margin: 0; line-height: 1.8;"><a href="servicebooking@lecastingparisien.com">servicebooking@lecastingparisien.com</a></p>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="widget">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="widget">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="widget">
                        <h5 class="widget-title font-alt"><?php echo $follow ?></h5>
                        <p style="font-size: large; text-align: center;"> &nbsp; <a href="https://www.facebook.com/LeCastingParisien/" target="_blank"><i class="fa fa-facebook"></i></a> &nbsp;<a href="https://www.pinterest.fr/" target="_blank"><i class="fa fa-pinterest"></i></a> &nbsp; <a href="https://www.instagram.com/lecastingparisien/" target="_blank"><i class="fa fa-instagram"></i></a> &nbsp;</p>
                    </div>
                </div>

            </div>

            <div style="padding: 5%"></div>

            <hr class="divider-d">

            <footer class="footer bg-dark" style="background: #000;">
                <div class="container">
                    <div class="row">
                        <p class="copyright font-alt" style="text-align: left;">&copy; 2020&nbsp;<a>Elody</a></p>
                    </div>
                </div>
            </footer>
        </div>

        <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div></main>

        <!-- JavaScripts -->
        <script src="assets/lib/jquery/dist/jquery.js"></script>
        <script src="assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="assets/lib/wow/dist/wow.js"></script>
        <script src="assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
        <script src="assets/lib/isotope/dist/isotope.pkgd.js"></script>
        <script src="assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
        <script src="assets/lib/flexslider/jquery.flexslider.js"></script>
        <script src="assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="assets/lib/smoothscroll.js"></script>
        <script src="assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
        <script src="assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/main.js"></script>
        <script>
            $(document).ready(function() {
                $('.js-scrollTo').on('click', function() {
                    var page = $(this).attr('href');
                    var speed = 750;
                    $('html, body').animate( { scrollTop: $(page).offset().top }, speed );
                    return false;
                });
            });
        </script>

        <!--Postulation, download image-->
        <script>
            loadImage = function(input,image) {
                var ext = $('#image_' + image).val().split('.').pop().toLowerCase();
                if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
                    $('#thumbnail_image_' + image).attr('src',"");
                } else {
                    if (input.files && input.files[0]) {
                        var fileInput = $('#image_' + image);
                        var maxSize = fileInput.data('max-size');
                        var fileSize = fileInput.get(0).files[0].size;
                        if(fileSize>maxSize){
                            $(".output").html('File size is more than 2mb.').fadeIn();
                            return false;
                        }
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#thumbnail_image_'+image).css('color','red');
                            $('#thumbnail_image_'+image).attr('src', e.target.result);
                            $('#thumbnail_image_'+image).css({
                                '-webkit-filter': 'none',
                                'filter': 'none',
                                'opacity': '1'
                            });
                        };
                        reader.readAsDataURL(input.files[0]);
                        var filename = input.files[0].name;
                    }else{
                        $('#thumbnail_image_' + image).attr('src',"");
                    }
                }
            };
        </script>

        </body>
    </html>

    <?php
}

//Fonction pour la partie française

//Fonctions pour la page d'accueil de "Index_vf"
function nav_bar() {
    ?>
    <!-- NavBar-->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation" style="width: 100%;">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="index_vf.php"><img src="media/about_us/LOGO2.png" width="70"></a>
            </div>

            <div class="collapse navbar-collapse" id="custom-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="index_vf.php?page=news">Actualités</a></li>
                    <li class="dropdown"><a class="js-scrollTo" href="#services">Services</a></li>
                    <li class="dropdown"><a class="js-scrollTo" href="#refe">Références</a></li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">À propos</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a class="js-scrollTo" href="#qui_sommes_nous">Qui sommes-nous ?</a>
                            <li class="dropdown"><a class="js-scrollTo" href="#que_faisons_nous">Que faisons-nous ?</a>   
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle js-scrollTo" href="#contact" data-toggle="dropdown">Contactez-nous</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_vf.php?page=postulation">Postulation</a>
                            <li class="dropdown"><a href="index_vf.php?page=ddv">Besoin d'un talent ?</a>   
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Langues</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_vf.php">Français</a>
                            <li class="dropdown"><a href="index_en.php">Anglais</a>   
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <?php
}

function video() {
    ?>
    <!-- Video Home-->

    <!--<section class="home-section bg-dark-30" data-background="media/about_us/img.png">
        <div class="video-player" data-property="{videoURL:'https://youtu.be/BbHeSq5xEY8', containment:'.home-section', mute:false, autoPlay:true, loop:false, opacity:1, showControls:false}">
        </div>
    </section>-->

    <section class="home-section home-full-height bg-dark-30" id="home" data-background="media/about_us/img.png">
        <div class="video-player" data-property="{videoURL:'http://elody.lescigales.org/LCP/media/LCP.mp4', containment:'.home-section', mute:false, autoPlay:true, loop:false, opacity:1}"></div>
      </section>
    <?php
}

function qui_sommes_nous() {
    ?>
    <!--Qui sommes-nous ? -->
    <section class="module pt-0 pb-0" id="qui_sommes_nous" style="margin-top: -1%;">
            <div class="row position-relative m-0">
                <div class="col-xs-12 col-md-6 side-image" data-background="media/about_us/anande_001.jpg"></div>
                <div class="col-xs-12 col-md-6 col-md-offset-6 side-image-text">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="module-title font-alt align-left">Qui sommes-nous ?</h2>
                            <div class="module-subtitle font-serif align-left">Une équipe unique de professionnels internationaux.</div>
                            <p style="text-align: justify;">Le Casting Parisien est une agence artistique qui s’est imposée et est devenue l’une des premières agences de casting. Le Casting Parisien représente fièrement une petite équipe qui choisit minutieusement les talents en fonction des critères de ses clients. En créant des stratégies et des campagnes de sourcing uniques sur vos critères de recherche, Le Casting Parisien source, détecte, pré sélectionne les profils les plus en phase avec votre image.</p>
                            <h3 class="font-serif align-left">Histoire</h3>
                            <p style="text-align: justify;">Créée en 2015 par Anande Cross, Le Casting Parisien est une agence internationale de casting, scouting et de détection de talent qui trouve et place des comédiens, talents spécifiques et mannequins publicitaires pour la publicité sous toutes ses différentes formes : films publicitaires, télévision, cinéma, support à la création vidéo, création de contenus digitaux, films d'entreprises. Depuis sa création nous avons trouvé plus de 40,000 artistes, talents mannequins trouvé en partenariat avec les agences en France et à l’international.</p>
                            <h3 class="font-serif align-left">Anande Cross, directrice de casting</h3>
                            <p style="text-align: justify;">Après avoir travaillée dans le Casting à Vancouver et Paris, elle s’est trouvée une passion pour l’industrie de la mode, lui donnant l’opportunité de parcourir Paris, New York, Tokyo et Milan. Travaillant avec des marques comme Dior, Louis Vuitton, Yohji Yamamoto, Jean-Paul Gaultier, etc... En 2006, elle lance Exelle Paris, une société de services et de personnel de première ligne qui s'est rapidement développée et est devenue une référence reconnue dans le domaine. Sollicitée par plusieurs marques de cosmétiques et de coiffure, elle crée Le Casting Parisien pour poursuivre sa première passion, le casting.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php
}

function services() {
    ?>
    <!-- Services -->
    <section class="module" style="padding-bottom: 0%;" id="services">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h4 class="font-alt mt-40 mb-0 module-title">Services</h4>
                        <hr class="divider-w mt-10 mb-20">
                        <div class="row multi-columns-row mb-70">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="content-box">
                                    <div class="content-box-image"><img class="animation_nb" src="media/about_us/evenement.jpg" alt="Title 1"/></div>
                                    <h3 class="content-box-title font-serif">Expertise</h3>
                                    <p style="text-align: justify;">Le Casting Parisien trouve mannequins, comédiens, sportifs, influenceurs, figurants, gameurs, acrobates, danseurs où tout talent particulier correspondant aux besoins des publicités, photos ou films publicitaires, défilés de mode, shootings, productions, campagnes cosmétiques, lancement de produit tout en mettant l’innovation technologique au service de votre image. Le Casting Parisien vous accompagne en parfaite conformité juridique grâce à une équipe unique et cosmopolite, aguerrie en gestion des droits à l’image et en prestations publicitaires.</p><a href="index_vf.php?page=postulation"><button class="btn btn-b btn-round" type="submit">Postulation</button></a>&nbsp;
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="content-box">
                                    <div class="content-box-image"><img class="animation_nb" src="media/about_us/expertise.jpg" alt="Title 2"/></div>
                                    <h3 class="content-box-title font-serif">Notre savoir faire</h3>
                                    <p style="text-align: justify;">Acteurs ? Mannequins ? Danseurs ? Gamers ? Acrobates ? Figurants ? Nous avons ce qu’il vous faut. Des comédiens pour une campagne de publicité ? Bien sûr ! N’hésitez pas à nous appeler quand vous avez un casting professionnel de prévu. A la recherche d’un mannequin pour un défilé à Paris ? On vous couvre ! Vous voulez un figurant à Rio ou Pékin pour un documentaire ? C’est OK ! Vous recherchez un cuisinier à Tokyo ? C'est comme si c'était fait ! Vous avez besoin d'un acteur à Londres ? C'est pour nous ! Tout ce que vous voulez, nous le trouvons !</p><a href="index_vf.php?page=ddv"><button class="btn btn-b btn-round" type="submit">Besoin d'un talent ?</button></a>&nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php
}

function que_faisons_nous() {
    ?>
    <!-- Que faisons-nous ? -->
    <h4 class="font-alt mt-40 mb-0 module-title" style="padding-top: 0%;" id="que_faisons_nous">Que faisons-nous ?</h4>
        <hr class="divider-w mt-10 mb-20">
        
        <section class="module bg-dark-60 pt-0 pb-0 parallax-bg testimonial" data-background="media/about_us/email.jpeg">
            <div class="testimonials-slider pt-140 pb-140">
                <ul class="slides">
                    <li><div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <h4 class="font-alt mt-40 mb-0 module-title">Casting</h4>
                                <hr class="divider-w mt-10 mb-20">
                                <p style="text-align: justify;">Que vous cherchiez comédien, artiste, mannequin professionnel, figurant, acteur, talent rare, new face pour défiler où poser; à Paris, en Europe ou dans le monde, nous les amènerons à vous. Notre expérience nous a permis d’acquérir un savoir-faire unique dans l’accompagnement des marques pour dénicher le talent le plus adéquat Mais également dans la détection de nouveaux visages en exerçant le casting de rue appelé aussi “scouting”. En créant votre stratégie digitale de sourcing nous orchestrons votre recherche de talents à l’échelle locale ou internationale, par le biais du Street Casting où tout autre technique de casting, aux quatre coins du monde aux campagnes digitales multi supports, avec une stratégie ad hoc pour détecter et capter les talents les plus rares. Le Casting Parisien met tout en oeuvre pour vous trouver les futurs visages de la mode, de la publicité, de la télévision avec créativité, transparence, efficacité et élégance.</p>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <h4 class="font-alt mt-40 mb-0 module-title">Casting de Rue</h4>
                                <hr class="divider-w mt-10 mb-20">
                                <p style="text-align: justify;">Le talent court les rues ! Que ce soit un new face pour les shows ou pour une campagne beauté ou publicité, l'équipe Le Casting Parisien adapte et déploie une recherche de talents spécialement conçue pour trouver les visages de demain et fait rayonner votre image dans un monde en constante évolution.</p>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <h4 class="font-alt mt-40 mb-0 module-title">Production et Direction Artistique</h4>
                                <hr class="divider-w mt-10 mb-20">
                                <p style="text-align: justify;">Pour donner vie à vos idées, vous souhaitez être épaulé et accompagné par des professionnels expérimentés qui sauront mettre en valeur votre image de marque, Le Casting Parisien recherche et unis les talents les plus rares de la mode et des spectacles. Avec plus de 25 ans d'expérience dans l'industrie de l'image et de la mode, Le Casting Parisien vous déniche les personnes les plus en phase avec votre projet dans les domaines de l’image; directeurs artistiques, photographes, et techniciens tout en respectant vos impératifs budgétaires.</p>
                            </div>
                        </div>
                    </div></li>
                </ul>
            </div>
        </section>

        <div style="padding: 2%;"></div>

    <?php
}

function carrousel() {
    ?>
    <!-- Carroussel -->
    <h4 class="font-alt mt-40 mb-0 module-title">La beauté n'a pas de frontière !</h4>
        <hr class="divider-w mt-10 mb-20">
        
        <section class="module bg-dark-60 pt-0 pb-0 parallax-bg testimonial" data-background="media/beauty_in_the_street/001.jpg">
            <div class="testimonials-slider ">
                <ul class="slides">
                    <li><img src="media/beauty_in_the_street/001.jpg" style="width: 100%; height: 100%;"></li>
                    <li><img src="media/beauty_in_the_street/002.jpg" style="width: 100%; height: 100%;"></li>
                    <li><img src="media/beauty_in_the_street/003.jpg" style="width: 100%; height: 100%;"></li>
                    <li><img src="media/beauty_in_the_street/004.jpg" style="width: 100%; height: 100%;"></li>
                    <li><img src="media/beauty_in_the_street/005.jpeg" style="width: 100%; height: 100%;"></li>
                </ul>
            </div>
        </section>
    <?php
}

function actu() {
    ?>
    <!--Actualités-->
    <section class="module">
            <div class="container">
                
                <div class="row">
                    <h4 class="font-alt mt-40 mb-0 module-title">Actualités</h4>
                    <hr class="divider-w mt-10 mb-20">
                    <p class="module-subtitle font-serif">Voici nos derniers castings sur lesquels nous avons travaillé à travers le monde entier. Une partie des belles campagnes de publicités et shooting que nous avons réalisé, montrant notre amour pour le casting !</p>
                </div>
                <div class="row multi-columns-row post-columns">

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><img class="animation_nb" src="media/services/V13_001.jpg" alt="Blog-post Thumbnail"/></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title">L'Oreal Paris Makeup</h2>
                                <div class="post-meta">Juin 2020</div>
                            </div>       
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><img class="animation_nb" src="media/services/V5_001.png" alt="Blog-post Thumbnail"/></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title">Défilé Stella McCartney</h2>
                                <div class="post-meta">Mars 2020</div>
                            </div>  
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><img class="animation_nb" src="media/services/V7_001.png" alt="Blog-post Thumbnail"/></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title">Vinted</h2>
                                <div class="post-meta">Juin 2020</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="module-small bg-dark" style="background: #000;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-8 col-lg-6 col-lg-offset-2">
                        <div class="callout-text font-alt">
                            <h3 class="callout-title">Vous voulez en voir plus ?</h3>
                            <p>C'est par ici !</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-2">
                        <div class="callout-btn-box"><a class="btn btn-w btn-round" href="index_vf.php?page=news">Venez voir notre actualité !</a></div>
                    </div>
                </div>
            </div>
        </section>
    <?php
}

function refe() {
    ?>
    <!-- Références -->
    <section class="module" id="refe">
            <div class="container">
                <div class="row">
                    <h4 class="font-alt mt-40 mb-0 module-title">Références</h4>
                    <hr class="divider-w mt-10 mb-20">
                    <div class="module-subtitle font-serif"></div>
                </div>

                <div class="row multi-columns-row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/andre.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/coca.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/diesel.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/etam.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/jpg.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/kenzo.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/lacoste.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/oasis.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/oreal.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/provost.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/publicis.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/saintlaurent.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/sas.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/sch.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/HSBC.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/jaguar.png" width=150></div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    <?php
}

function index_vf() {
    entete("Le Casting Parisien, la meilleure agence de Paris");
    nav_bar();
    video();
    qui_sommes_nous();
    services();
    que_faisons_nous();
    carrousel();
    actu();
    refe();
    pied("Nous suivre");
}

//Fonctions pour la page d'actualités
function nav_bar_2() {
    ?>
    <!-- NavBar-->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="index_vf.php"><img src="media/about_us/LOGO2.png" width="70"></a>
            </div>

            <div class="collapse navbar-collapse" id="custom-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="index_vf.php?page=news">Actualités</a></li>
                    <li class="dropdown"><a href="index_vf.php#services">Services</a></li>
                    <li class="dropdown"><a href="index_vf.php#refe">Références</a></li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">À propos</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_vf.php#qui_sommes_nous">Qui sommes-nous ?</a>
                            <li class="dropdown"><a href="index_vf.php#que_faisons_nous">Que faisons-nous ?</a>   
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Contactez-nous</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_vf.php?page=postulation">Postulation</a>
                            <li class="dropdown"><a href="index_vf.php?page=ddv">Besoin d'un talent ?</a>   
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Langues</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_vf.php">Français</a>
                            <li class="dropdown"><a href="index_en.php">Anglais</a>   
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <?php
}

function actu_2() {
    ?>
    <section class="module bg-dark-60 blog-page-header song" data-background="media/about_us/img.png">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3" style="width: 100%; margin-left: 0%;">
                    <h2 class="module-title font-alt" style="margin-top: 8%; margin-bottom: 11%;">Actualités</h2>
                    <p class="module-subtitle font-serif">Voici nos derniers castings sur lesquels nous avons travaillé à travers le monde entier. Une partie des belles campagnes de publicités et shooting que nous avons réalisé, montrant notre amour pour le casting !</p>
              </div>
            </div>
        </div>
    </section>

    <section class="module">
        <div class="container">
            <div class="row post-masonry post-columns">

                <!-- Philips -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V17.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Dominique pour Philips</h2>
                            <div class="post-meta">Novembre 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><video autoplay muted style="width:100%"><source src="media/services/V18.mp4" type="video/mp4"></video></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Dominique pour Philips</h2>
                            <div class="post-meta">Novembre 2020</div>
                        </div>
                    </div>
                </div>


                <!-- L'Oreal Paris Makeup-->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V13.jpg" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">L'Oreal Paris Makeup</h2>
                            <div class="post-meta">Juin 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V14.jpg" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">L'Oreal Paris Makeup</h2>
                            <div class="post-meta">Juin 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><video autoplay muted style="width:100%"><source src="media/services/V15.mp4" type="video/mp4"></video></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">L'Oreal Paris Makeup</h2>
                            <div class="post-meta">Juin 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><video autoplay muted style="width:100%"><source src="media/services/V16.mp4" type="video/mp4"></video></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">L'Oreal Paris Makeup</h2>
                            <div class="post-meta">Juin 2020</div>
                        </div>
                    </div>
                </div>

                <!-- Vinted 2020 -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V12.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, campagne de pub France-Belgique</h2>
                            <div class="post-meta">Juin 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V7.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, campagne de pub France-Belgique</h2>
                            <div class="post-meta">Juin 2020</div>
                        </div>
                    </div>
                </div>

                <!-- Défilé Stella McCartney -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V4.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Défilé Stella McCartney</h2>
                            <div class="post-meta">Mars 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V5.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Défilé Stella McCartney</h2>
                            <div class="post-meta">Mars 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V6.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Défilé Stella McCartney</h2>
                            <div class="post-meta">Mars 2020</div>
                        </div>
                    </div>
                </div>

                <!-- Vanish -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V1.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vanish Gold Commercial</h2>
                            <div class="post-meta">Mai 2019</div>
                        </div>
                    </div>
                </div>

                <!-- Vanish -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V2.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">The Tanneur</h2>
                            <div class="post-meta">Juin 2017</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V8.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                        <h2 class="post-title">The Tanneur</h2>
                            <div class="post-meta">Juin 2017</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V9.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">The Tanneur</h2>
                            <div class="post-meta">Juin 2017</div>
                        </div>
                    </div>
                </div>


                <!-- Vinted 2016 -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V10.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, campagne de pub France-Belgique</h2>
                            <div class="post-meta">Mars 2016</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V3.jpg" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, campagne de pub France-Belgique</h2>
                            <div class="post-meta">Mars 2016</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V11.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, campagne de pub France-Belgique</h2>
                            <div class="post-meta">Mars 2016</div>
                        </div>
                    </div>
                </div>
        </section>

    <?php

}

function news_vf() {
    entete("Le Casting Parisien, la meilleure agence de Paris");
    nav_bar_2();
    actu_2();
    pied("Nous suivre");
}

//Fonctions pour la page de postulation
function postulation_section_vf() {
    ?>
    <!--Formulaire-->
    <section class="module bg-dark-60 blog-page-header song" data-background="media/about_us/img.png">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3" style="width: 100%; margin-left: 0%;">
                    <h2 class="module-title font-alt" style="margin-top: 8%; margin-bottom: 11%;">Faîtes-vous repérer !</h2>
                    <p class="module-subtitle font-serif">Votre chance de briller</p>
              </div>
            </div>
        </div>
    </section>

    <section class="module">
        <div class="container">  

            <?php
            if($_POST) {
                // if(mail($email_to, $subject, $message, $headers)) {
                if(true) {
                    echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Alerte !</strong> Votre demande a été envoyée !</div>";
                } else {
                    echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Alerte !</strong> Votre demande n'a pas été envoyée !</div>";
                }
            }
            ?>

        <form id="Form" method="post" enctype="multipart/form-data" action="index_vf.php?page=postulation">
            <table class="taille_table">
                <tr>
                    <td><input class="no-border" type="text" id="firstname" name="firstname" placeholder="PRÈNOM" required></td>
                    <td><input class="no-border" type="email" name="email" placeholder="EMAIL" required/></td>
                </tr>
                <tr>
                    <td><input class="no-border" type="text" name="surname" placeholder="NOM" required/></td>
                    <td><input class="no-border" type="tel" name="phoneNumber" placeholder="NUMÉRO DE TÉLÉPHONE" required/></td>
                </tr>
                <tr>
                    <td>
                        <select class="heightSelect" id="height" name="height" required>
                           <option value="" selected>TAILLE</option>
                            <option value="160cm">160cm</option>
                            <option value="161cm">161cm</option>
                            <option value="162cm">162cm</option>
                            <option value="163cm">163cm</option>
                            <option value="164cm">164cm</option>
                            <option value="165cm">165cm</option>
                            <option value="166cm">166cm</option>
                            <option value="167cm">167cm</option>
                            <option value="168cm">168cm</option>
                            <option value="169cm">169cm</option>
                            <option value="170cm">170cm</option>
                            <option value="171cm">171cm</option>
                            <option value="172cm">172cm</option>
                            <option value="173cm">173cm</option>
                            <option value="174cm">174cm</option>
                            <option value="175cm">175cm</option>
                            <option value="176cm">176cm</option>
                            <option value="177cm">177cm</option>
                            <option value="178cm">178cm</option>
                            <option value="179cm">179cm</option>
                            <option value="180cm">180cm</option>
                            <option value="181cm">181cm</option>
                            <option value="182cm">182cm</option>
                            <option value="183cm">183cm</option>
                            <option value="184cm">184cm</option>
                            <option value="185cm">185cm</option>
                            <option value="186cm">186cm</option>
                            <option value="187cm">187cm</option>
                            <option value="188cm">188cm</option>
                            <option value="189cm">189cm</option>
                            <option value="190cm">190cm</option>
                            <option value="191cm">191cm</option>
                            <option value="192cm">192cm</option>
                            <option value="193cm">193cm</option>
                            <option value="194cm">194cm</option>
                            <option value="195cm">195cm</option>
                            <option value="196cm">196cm</option>
                            <option value="197cm">197cm</option>
                            <option value="198cm">198cm</option>
                            <option value="199cm">199cm</option>
                            <option value="200cm">200cm</option>
                            <option value="201cm">201cm</option>
                            <option value="202cm">202cm</option>
                            <option value="203cm">203cm</option>
                            <option value="204cm">204cm</option>
                            <option value="205cm">205cm</option>
                            <option value="206cm">206cm</option>
                            <option value="207cm">207cm</option>
                            <option value="208cm">208cm</option>
                            <option value="209cm">209cm</option>
                            <option value="210cm">210cm</option>
                        </select>
                    </td>
                    <td><input class="no-border" type="text" name="city" placeholder="PAYS" required/></td>
                </tr>
                <tr>
                    <td>
                        <select class="genderSelect" id="gender" name="gender" required>
                            <option value="" selected>SEXE</option>
                            <option value="Man">Homme</option>
                            <option value="Woman">Femme</option>
                        </select>
                    </td>
                    <td><input class="no-border" type="text" name="instagram" placeholder="INSTAGRAM" id="instagramBecome" required></td>
                </tr>
                <tr>
                    <td>
                        <div class="becomeListElement">
                            <select id="day" name="day" class="daySelect" required>
                                <option value="" selected>JOUR</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>

                            <select class="applyFormInput inputRow monthSelect" id="month" name="month" required>
                                <option value="" selected>MOIS</option>
                                <option value="January">Janvier</option>
                                <option value="February">Février</option>
                                <option value="March">Mars</option>
                                <option value="April">Avril</option>
                                <option value="May">Mai</option>
                                <option value="June">Juin</option>
                                <option value="July">Juillet</option>
                                <option value="August">Août</option>
                                <option value="September">Septembre</option>
                                <option value="October">Octobre</option>
                                <option value="November">Novembre</option>
                                <option value="December">Décembre</option>
                            </select>

                            <select class="applyFormInput inputRow yearSelect" id="year" name="year" required>
                                <option value="" selected>ANNÉE</option>
                                <option value="1970">1950</option>
                                <option value="1971">1951</option>
                                <option value="1972">1952</option>
                                <option value="1973">1953</option>
                                <option value="1974">1954</option>
                                <option value="1975">1955</option>
                                <option value="1976">1956</option>
                                <option value="1977">1957</option>
                                <option value="1978">1958</option>
                                <option value="1979">1959</option>
                                <option value="1980">1960</option>
                                <option value="1981">1961</option>
                                <option value="1982">1962</option>
                                <option value="1983">1963</option>
                                <option value="1984">1964</option>
                                <option value="1985">1965</option>
                                <option value="1986">1966</option>
                                <option value="1987">1967</option>
                                <option value="1988">1968</option>
                                <option value="1989">1969</option>
                                <option value="1970">1970</option>
                                <option value="1971">1971</option>
                                <option value="1972">1972</option>
                                <option value="1973">1973</option>
                                <option value="1974">1974</option>
                                <option value="1975">1975</option>
                                <option value="1976">1976</option>
                                <option value="1977">1977</option>
                                <option value="1978">1978</option>
                                <option value="1979">1979</option>
                                <option value="1980">1980</option>
                                <option value="1981">1981</option>
                                <option value="1982">1982</option>
                                <option value="1983">1983</option>
                                <option value="1984">1984</option>
                                <option value="1985">1985</option>
                                <option value="1986">1986</option>
                                <option value="1987">1987</option>
                                <option value="1988">1988</option>
                                <option value="1989">1989</option>
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                                <option value="1994">1994</option>
                                <option value="1995">1995</option>
                                <option value="1996">1996</option>
                                <option value="1997">1997</option>
                                <option value="1998">1998</option>
                                <option value="1999">1999</option>
                                <option value="2000">2000</option>
                                <option value="2001">2001</option>
                                <option value="2002">2002</option>
                                <option value="2003">2003</option>
                                <option value="2004">2004</option>
                                <option value="2005">2005</option>
                                <option value="2006">2006</option>
                                <option value="2007">2007</option>
                                <option value="2008">2008</option>
                                <option value="2009">2009</option>
                                <option value="2010">2010</option>
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                            </select>
                        </div>
                    </td>
                    <td><input class="no-border" type="text" name="facebook" placeholder="FACEBOOK" id="facebookBecome" required></td>
                </tr>
            </table>

            <div class="upload">
                <div class="imageLoader">
                    <img id="thumbnail_image_1" src="media/postulation/upload1.png" alt="become image 1" style="width: 100%;">
                    <div class="overlay">
                        <div class="uploadLogo"><img src="media/postulation/upload-logo.png" /></div>
                    </div>
                    <input name="image_1" id="image_1" accept=".png,.jpg,.jpeg" type='file' class="uploadButton" onchange="loadImage(this,1)" required>
                </div>

                <div class="imageLoader">
                    <img id="thumbnail_image_2" src="media/postulation/upload2.png"  alt="become image 2" style="width: 100%;">
                    <div class="overlay">
                        <div class="uploadLogo"><img src="media/postulation/upload-logo.png" /></div>
                    </div>
                    <input name="image_2" id="image_2" accept=".png,.jpg,.jpeg" type='file' class="uploadButton" onchange="loadImage(this,2)" required/>
                </div>

                <div class="imageLoader">
                    <img id="thumbnail_image_3" src="media/postulation/upload3.png" alt="become image 3" style="width: 100%;">
                    <div class="overlay">
                        <div class="uploadLogo"><img src="media/postulation/upload-logo.png" /></div>
                    </div>
                    <input name="image_3" id="image_3" accept=".png,.jpg,.jpeg" type='file' class="uploadButton" onchange="loadImage(this,3)" required/>
                </div>

                <div class="imageLoader">
                    <img id="thumbnail_image_4" src="media/postulation/upload4.png" alt="become image 4" style="width: 100%;">
                    <div class="overlay">
                        <div class="uploadLogo "><img src="media/postulation/upload-logo.png" /></div>
                    </div>
                    <input name="image_4" id="image_4" accept=".png,.jpg,.jpeg" type='file' class="uploadButton" onchange="loadImage(this,4)" required/>
                </div>

            </div>

            <div class="buttonSubmit">
                <button  class="becomeSubmit" type="submit" >ENVOYER</button>
            </div>
        </form>
        
        </div>
    </section>
    <?php
}

function postulation_vf() {
    entete("Le Casting Parisien, la meilleure agence de Paris");
    nav_bar_2();
    postulation_section_vf();
    pied("Nous suivre");
}

//Fonctions pour la page de demande de devis
function ddv_section_vf() {
    ?>
    <!--Formulaire-->
    <section class="module bg-dark-60 blog-page-header song" data-background="media/about_us/img.png">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3" style="width: 100%; margin-left: 0%;">
                    <h2 class="module-title font-alt" style="margin-top: 8%; margin-bottom: 11%;">Demande de devis !</h2>
                    <p class="module-subtitle font-serif">Téléchargez notre <a href="PRES.pdf" style="color: #ffffff;">présentation ici</a>.</p>
              </div>
            </div>
        </div>
    </section>

    <section class="module">
        <div class="container">

        <?php
        if($_POST) {
            // if(mail($email_to, $subject, $message, $headers)) {
            if(true) {
                echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Alerte !</strong> Votre demande a été envoyée !</div>";
            } else {
                echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Alerte !</strong> Votre demande n'a pas été envoyée !</div>";
            }
        }
        ?>

        <form id="Form" method="post" action="index_vf.php?page=ddv">
            <table style="margin-left: -8%;">
                <tr>
                    <td><input class="no-border" type="text" name="firstname" placeholder="PRÈNOM" required></td>
                    <td><input class="no-border" type="text" name="surname" placeholder="NOM" required></td>
                    </tr>
                <tr>
                    <td><input class="no-border" type="text" name="email" placeholder="EMAIL" required></td>
                    <td><input class="no-border" type="text" name="name_entreprise" placeholder="NOM DE L'ENTREPRISE" required></td>
                </tr>
                <tr>
                    <td><input class="no-border" type="tel" name="phoneNumber" placeholder="NUMÉRO DE TÉLÉPHONE" required></td>
                    <td><textarea class="no-border" name="demande" style="width: 100%" maxlength='2000' placeholder="VOTRE DEMANDE (2.000)" required></textarea></td>
                </tr>
            </table>
                    
            <div class="buttonSubmit">
                <button  class="becomeSubmit" type="submit" >ENVOYER</button>
            </div>
        </form>
        </div>
    </section>
    <?php
}

function ddv_vf() {
    entete("Le Casting Parisien, la meilleure agence de Paris");
    nav_bar_2();
    ddv_section_vf();
    pied("Nous suivre");
}


//Fonction pour la partie anglaise
//Fonctions pour la page d'accueil de "Index_en"
function nav_bar_en() {
    ?>
    <!-- NavBar-->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="index_en.php"><img src="media/about_us/LOGO2.png" width="70"></a>
            </div>

            <div class="collapse navbar-collapse" id="custom-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="index_en.php?page=news">Latest News</a></li>
                    <li class="dropdown"><a class="js-scrollTo" href="#services">Services</a></li>
                    <li class="dropdown"><a class="js-scrollTo" href="#refe">References</a></li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">About Us</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a class="js-scrollTo" href="#qui_sommes_nous">Who we are ?</a>
                            <li class="dropdown"><a class="js-scrollTo" href="#que_faisons_nous">What we do ?</a>   
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle js-scrollTo" href="#contact" data-toggle="dropdown">Contact Us</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_en.php?page=postulation">Postulation</a>
                            <li class="dropdown"><a href="index_en.php?page=ddv">Need a talent ?</a>   
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Languages</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_vf.php">French</a>
                            <li class="dropdown"><a href="index_en.php">English</a>   
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <?php
}

function who_we_are() {
    ?>
    <!--Qui sommes-nous ? -->
    <section class="module pt-0 pb-0" id="qui_sommes_nous" style="margin-top: -1%;">
            <div class="row position-relative m-0">
                <div class="col-xs-12 col-md-6 side-image" data-background="media/about_us/anande_001.jpg"></div>
                <div class="col-xs-12 col-md-6 col-md-offset-6 side-image-text">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="module-title font-alt align-left">Who we are ?</h2>
                            <div class="module-subtitle font-serif align-left">A Unique Team of International Professionals.</div>
                            <p style="text-align: justify;">Le Casting Parisien is an artistic agency that has established itself and become one of the first casting agencies. Le Casting Parisien proudly represents a small team who choose closely its talents regarding the criterias of its clients.</p>
                            <h3 class="font-serif align-left">Story</h3>
                            <p style="text-align: justify;">Created by Anande Cross in 2015 Le Casting Parisien is specialises in talent detection, scouting and casting for model and talent placement, for creative content of all kinds : advertising shots; television, cinema, and internet commercials, runway, cinema corporate films, in France and internationally. With more than 40,000 talents already found and more every day in partnership with talents and model agencies in Paris and worldwide.</p>
                            <h3 class="font-serif align-left">Anande Cross, Casting Director</h3>
                            <p style="text-align: justify;">After working in Casting in Vancouver and Paris, she found herself working in the fashion industry, taking her to Paris, New-York, Tokyo and Milan working on shows such as Dior, Louis Vuitton, Prada, Comme des Garcons, Yohji Yamamoto, Yves St Laurent, Jean-Paul Gaultier, Berlutti, Etam, Zuhair Murad, etc...alongside Pat McGrath, Lloyd Simmonds and Corinne Liscia. In 2006, she launched Exelle Paris, a Service and Front Line Personnel Company which quickly grew and is a renowned reference in the field.Requested by multiple brands in cosmetics and hair, she created Le Casting Parisien to pursue her first passion, casting.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php
}

function services_en() {
    ?>
    <!-- Services -->
    <section class="module" style="padding-bottom: 0%;" id="services">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h4 class="font-alt mt-40 mb-0 module-title">Services</h4>
                        <hr class="divider-w mt-10 mb-20">
                        <div class="row multi-columns-row mb-70">
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="content-box">
                                    <div class="content-box-image"><img class="animation_nb" src="media/about_us/evenement.jpg" alt="Title 1"/></div>
                                    <h3 class="content-box-title font-serif">Expertise</h3>
                                    <p style="text-align: justify;">Le Casting Parisien finds models, comedians, athletics, influencers, gamers, dancers or any particular talents corresponding to the needs of advertisements, photos or advertising movies, fashion shows, shootings, productions, directors, digital recording, movies extras, cosmetics campaigns, institutional campaigns, product launch, digital content by putting technological innovation at the service of your brand, the full range of services. Le Casting Parisien supports you in perfect legal compliance.</p><a href="index_en.php?page=postulation"><button class="btn btn-b btn-round" type="submit">Postulation</button></a>&nbsp;
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6">
                                <div class="content-box">
                                    <div class="content-box-image"><img class="animation_nb" src="media/about_us/expertise.jpg" alt="Title 2"/></div>
                                    <h3 class="content-box-title font-serif">What we do & How</h3>
                                    <p style="text-align: justify;">Actors ? Models ? Dancers ? Gamers ? Acrobats ? Extras ? We've got just the thing for you. Actors for a publicity campaign ? Of course we have actors for an ad campaign. Don't hesitate to call us when you have a professional casting scheduled. Looking for a model for a fashion show in Paris ? We've got you covered ! Want an extra in Rio or Beijing for a documentary? It's OK! Looking for a cook in Tokyo ? It's a done deal ! Need an actor in London ? It's all ours ! Whatever you want, we'll find it !</p><a href="index_en.php?page=ddv"><button class="btn btn-b btn-round" type="submit">Need a talent ?</button></a>&nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php
}

function what_we_do() {
    ?>
    <!-- Que faisons-nous ? -->
    <h4 class="font-alt mt-40 mb-0 module-title" style="padding-top: 0%;" id="que_faisons_nous">What we do ?</h4>
        <hr class="divider-w mt-10 mb-20">
        
        <section class="module bg-dark-60 pt-0 pb-0 parallax-bg testimonial" data-background="media/about_us/email.jpeg">
            <div class="testimonials-slider pt-140 pb-140">
                <ul class="slides">
                    <li><div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <h4 class="font-alt mt-40 mb-0 module-title">Casting</h4>
                                <hr class="divider-w mt-10 mb-20">
                                <p style="text-align: justify;">Whether you need a real person, a professional model, an extra, an actor or a niche talent, or a new face for couture right off the street, in Paris, Europe or worldwide, if they are out there, we will bring them to you. Our unique experience means we cater to high-end brands in cosmetics as well as street casting for brand marketing. By creating your very digital sourcing strategy for your national or international talent search, from the Street Casting in international cities to massive SEO driven digital targeted campaigns, Le Casting Parisien finds the faces for advertising, fashion, television & runway with creativity, transparency, efficiency and elegance.</p>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <h4 class="font-alt mt-40 mb-0 module-title">Street Casting</h4>
                                <hr class="divider-w mt-10 mb-20">
                                <p style="text-align: justify;">Talent is in the street ! Whether it’s a new face for catwalks or beauty campaigns, Le Casting Parisien team will tailor and roll out a talent search specifically designed to meet your needs and will keep your image fresh and responsive to ever shifting trends.</p>
                            </div>
                        </div>
                    </div></li>
                    <li><div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <h4 class="font-alt mt-40 mb-0 module-title">Scouting & DA</h4>
                                <hr class="divider-w mt-10 mb-20">
                                <p style="text-align: justify;">To piece your project together you need an experienced professional to bring your idea to life. With over 20 years experience in the image and fashion industry, Le Casting Parisien will find you the best photographers, art directors and technicians for a price that works for you.</p>
                            </div>
                        </div>
                    </div></li>
                </ul>
            </div>
        </section>

        <div style="padding: 2%;"></div>

    <?php
}

function carrousel_en() {
    ?>
    <!-- Carroussel -->
    <h4 class="font-alt mt-40 mb-0 module-title">The beauty in the street</h4>
        <hr class="divider-w mt-10 mb-20">
        
        <section class="module bg-dark-60 pt-0 pb-0 parallax-bg testimonial" data-background="media/beauty_in_the_street/001.jpg">
            <div class="testimonials-slider ">
                <ul class="slides">
                    <li><img src="media/beauty_in_the_street/001.jpg" style="width: 100%; height: 100%;"></li>
                    <li><img src="media/beauty_in_the_street/002.jpg" style="width: 100%; height: 100%;"></li>
                    <li><img src="media/beauty_in_the_street/003.jpg" style="width: 100%; height: 100%;"></li>
                    <li><img src="media/beauty_in_the_street/004.jpg" style="width: 100%; height: 100%;"></li>
                    <li><img src="media/beauty_in_the_street/005.jpeg" style="width: 100%; height: 100%;"></li>
                </ul>
            </div>
        </section>
    <?php
}

function actu_en() {
    ?>
    <!--Actualités-->
    <section class="module">
            <div class="container">
                
                <div class="row">
                    <h4 class="font-alt mt-40 mb-0 module-title">Latest News</h4>
                    <hr class="divider-w mt-10 mb-20">
                    <p class="module-subtitle font-serif">Here are our latest castings that we have been working on around the world. Some of the beautiful advertising campaigns and shootings we've done, showing our love for casting !</p>
                </div>
                <div class="row multi-columns-row post-columns">

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><img class="animation_nb" src="media/services/V13_001.jpg" alt="Blog-post Thumbnail"/></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title">L'Oreal Paris Makeup</h2>
                                <div class="post-meta">June 2020</div>
                            </div>       
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><img class="animation_nb" src="media/services/V5_001.png" alt="Blog-post Thumbnail"/></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title">Parade Stella McCartney</h2>
                                <div class="post-meta">March 2020</div>
                            </div>                                  
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="post mb-20">
                            <div class="post-thumbnail"><img class="animation_nb" src="media/services/V7_001.png" alt="Blog-post Thumbnail"/></div>
                            <div class="post-header font-alt">
                                <h2 class="post-title">Vinted</h2>
                                <div class="post-meta">June 2020</div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="module-small bg-dark" style="background: #000;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-8 col-lg-6 col-lg-offset-2">
                        <div class="callout-text font-alt">
                            <h3 class="callout-title">Want to see more ?</h3>
                            <p>It's this way !</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-2">
                        <div class="callout-btn-box"><a class="btn btn-w btn-round" href="index_en.php?page=news">Come see our news !</a></div>
                    </div>
                </div>
            </div>
        </section>
    <?php
}

function refe_en() {
    ?>
    <!-- Références -->
    <section class="module" id="refe">
            <div class="container">
                <div class="row">
                    <h4 class="font-alt mt-40 mb-0 module-title">References</h4>
                    <hr class="divider-w mt-10 mb-20">
                    <div class="module-subtitle font-serif"></div>
                </div>

                <div class="row multi-columns-row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/andre.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/coca.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/diesel.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/etam.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/jpg.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/kenzo.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/lacoste.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/oasis.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/oreal.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/provost.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/publicis.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/saintlaurent.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/sas.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/sch.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/HSBC.png" width=150></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="features-item">
                            <div class="features-icon"><img class="animation_nb" src="media/logo_customers/jaguar.png" width=150></div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    <?php
}

function index_en() {
    entete("Le Casting Parisien, the best agency of Paris");
    nav_bar_en();
    video();
    who_we_are();
    services_en();
    what_we_do();
    carrousel_en();
    actu_en();
    refe_en();
    pied("Follow Us");
}

//Fonctions pour la page d'actualités
function nav_bar_2_en() {
    ?>
    <!-- NavBar-->
    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="index_en.php"><img src="media/about_us/LOGO2.png" width="70"></a>
            </div>

            <div class="collapse navbar-collapse" id="custom-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="index_en.php?page=news">Latest News</a></li>
                    <li class="dropdown"><a href="index_en.php#services">Services</a></li>
                    <li class="dropdown"><a href="index_en.php#refe">References</a></li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">About Us</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_en.php#qui_sommes_nous">Who we are ?</a>
                            <li class="dropdown"><a href="index_en.php#que_faisons_nous">What we do ?</a>   
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Contact Us</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_en.php?page=postulation">Postulation</a>
                            <li class="dropdown"><a href="index_en.php?page=ddv">Need a talent ?</a>   
                        </ul>
                    </li>

                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">Languages</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="index_vf.php">French</a>
                            <li class="dropdown"><a href="index_en.php">English</a>   
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <?php
}

function actu_2_en() {
    ?>
    <section class="module bg-dark-60 blog-page-header song" data-background="media/about_us/img.png">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3" style="width: 100%; margin-left: 0%;">
                    <h2 class="module-title font-alt" style="margin-top: 8%; margin-bottom: 11%;">Latest News</h2>
                    <p class="module-subtitle font-serif">Here are our latest castings that we have been working on around the world. Some of the beautiful advertising campaigns and shootings we've done, showing our love for casting !</p>
              </div>
            </div>
        </div>
    </section>

    <section class="module">
        <div class="container">
            <div class="row post-masonry post-columns">

                <!-- Philips -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V17.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Dominique for Philips</h2>
                            <div class="post-meta">November 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><video autoplay muted style="width:100%"><source src="media/services/V18.mp4" type="video/mp4"></video></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Dominique for Philips</h2>
                            <div class="post-meta">November 2020</div>
                        </div>
                    </div>
                </div>


                <!-- L'Oreal Paris Makeup-->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V13.jpg" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">L'Oreal Paris Makeup</h2>
                            <div class="post-meta">June 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V14.jpg" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">L'Oreal Paris Makeup</h2>
                            <div class="post-meta">June 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><video autoplay muted style="width:100%"><source src="media/services/V15.mp4" type="video/mp4"></video></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">L'Oreal Paris Makeup</h2>
                            <div class="post-meta">June 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><video autoplay muted style="width:100%"><source src="media/services/V16.mp4" type="video/mp4"></video></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">L'Oreal Paris Makeup</h2>
                            <div class="post-meta">June 2020</div>
                        </div>
                    </div>
                </div>

                <!-- Vinted 2020 -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V12.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, advertising campaign France-Belgium,</h2>
                            <div class="post-meta">June 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V7.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, advertising campaign France-Belgium,</h2>
                            <div class="post-meta">June 2020</div>
                        </div>
                    </div>
                </div>

                <!-- Défilé Stella McCartney -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V4.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Parade Stella McCartney</h2>
                            <div class="post-meta">March 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V5.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Parade Stella McCartney</h2>
                            <div class="post-meta">March 2020</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V6.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Parade Stella McCartney</h2>
                            <div class="post-meta">March 2020</div>
                        </div>
                    </div>
                </div>

                <!-- Vanish -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V1.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vanish Gold Commercial</h2>
                            <div class="post-meta">May 2019</div>
                        </div>
                    </div>
                </div>

                <!-- Vanish -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V2.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">The Tanneur</h2>
                            <div class="post-meta">June 2017</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V8.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                        <h2 class="post-title">The Tanneur</h2>
                            <div class="post-meta">June 2017</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V9.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">The Tanneur</h2>
                            <div class="post-meta">June 2017</div>
                        </div>
                    </div>
                </div>


                <!-- Vinted 2016 -->
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V10.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, advertising campaign France-Belgium,</h2>
                            <div class="post-meta">March 2016</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V3.jpg" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, advertising campaign France-Belgium,</h2>
                            <div class="post-meta">March 2016</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post">
                        <div class="post-thumbnail"><img class="animation_nb" style="width:100%;" src="media/services/V11.png" alt="Blog-post Thumbnail"/></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title">Vinted, advertising campaign France-Belgium,</h2>
                            <div class="post-meta">March 2016</div>
                        </div>
                    </div>
                </div>
        </section>

    <?php

}

function news_en() {
    entete("Le Casting Parisien, the best agency of Paris");
    nav_bar_2_en();
    actu_2_en();
    pied("Follow Us");
}

//Fonctions pour la page de postulation
function postulation_section_en() {
    ?>
    <!--Formulaire-->
    <section class="module bg-dark-60 blog-page-header song" data-background="media/about_us/img.png">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3" style="width: 100%; margin-left: 0%;">
                    <h2 class="module-title font-alt" style="margin-top: 8%; margin-bottom: 11%;">Get Spotted !</h2>
                    <p class="module-subtitle font-serif">Your chance to shine</p>
              </div>
            </div>
        </div>
    </section>

    <section class="module">
        <div class="container">  

            <?php
            if($_POST) {
                // if(mail($email_to, $subject, $message, $headers)) {
                if(true) {
                    echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Alert !</strong> Your request has been sent.</div>";
                } else {
                    echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Alert !</strong> Your request has not been sent, please fill in the form correctly.</div>";
                }
            }
            ?>

        <form id="Form" method="post" enctype="multipart/form-data" action="index_en.php?page=postulation">
            <table class="taille_table">
                <tr>
                    <td><input class="no-border" type="text" id="firstname" name="firstname" placeholder="FIRSTNAME" required></td>
                    <td><input class="no-border" type="email" name="email" placeholder="EMAIL" required/></td>
                </tr>
                <tr>
                    <td><input class="no-border" type="text" name="surname" placeholder="SURNAME" required/></td>
                    <td><input class="no-border" type="tel" name="phoneNumber" placeholder="PHONE NUMBER" required/></td>
                </tr>
                <tr>
                    <td>
                        <select class="heightSelect" id="height" name="height" required>
                           <option value="" selected>HEIGHT</option>
                            <option value="160cm">160cm</option>
                            <option value="161cm">161cm</option>
                            <option value="162cm">162cm</option>
                            <option value="163cm">163cm</option>
                            <option value="164cm">164cm</option>
                            <option value="165cm">165cm</option>
                            <option value="166cm">166cm</option>
                            <option value="167cm">167cm</option>
                            <option value="168cm">168cm</option>
                            <option value="169cm">169cm</option>
                            <option value="170cm">170cm</option>
                            <option value="171cm">171cm</option>
                            <option value="172cm">172cm</option>
                            <option value="173cm">173cm</option>
                            <option value="174cm">174cm</option>
                            <option value="175cm">175cm</option>
                            <option value="176cm">176cm</option>
                            <option value="177cm">177cm</option>
                            <option value="178cm">178cm</option>
                            <option value="179cm">179cm</option>
                            <option value="180cm">180cm</option>
                            <option value="181cm">181cm</option>
                            <option value="182cm">182cm</option>
                            <option value="183cm">183cm</option>
                            <option value="184cm">184cm</option>
                            <option value="185cm">185cm</option>
                            <option value="186cm">186cm</option>
                            <option value="187cm">187cm</option>
                            <option value="188cm">188cm</option>
                            <option value="189cm">189cm</option>
                            <option value="190cm">190cm</option>
                            <option value="191cm">191cm</option>
                            <option value="192cm">192cm</option>
                            <option value="193cm">193cm</option>
                            <option value="194cm">194cm</option>
                            <option value="195cm">195cm</option>
                            <option value="196cm">196cm</option>
                            <option value="197cm">197cm</option>
                            <option value="198cm">198cm</option>
                            <option value="199cm">199cm</option>
                            <option value="200cm">200cm</option>
                            <option value="201cm">201cm</option>
                            <option value="202cm">202cm</option>
                            <option value="203cm">203cm</option>
                            <option value="204cm">204cm</option>
                            <option value="205cm">205cm</option>
                            <option value="206cm">206cm</option>
                            <option value="207cm">207cm</option>
                            <option value="208cm">208cm</option>
                            <option value="209cm">209cm</option>
                            <option value="210cm">210cm</option>
                        </select>
                    </td>
                    <td><input class="no-border" type="text" name="city" placeholder="CITY" required/></td>
                </tr>
                <tr>
                    <td>
                        <select class="genderSelect" id="gender" name="gender" required>
                            <option value="" selected>Gender</option>
                            <option value="Man">Man</option>
                            <option value="Woman">Woman</option>
                        </select>
                    </td>
                    <td><input class="no-border" type="text" name="instagram" placeholder="INSTAGRAM" id="instagramBecome" required></td>
                </tr>
                <tr>
                    <td>
                        <div class="becomeListElement">
                            <select id="day" name="day" class="daySelect" required>
                                <option value="" selected>DAY</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>

                            <select class="applyFormInput inputRow monthSelect" id="month" name="month" required>
                                <option value="" selected>MONTH</option>
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>

                            <select class="applyFormInput inputRow yearSelect" id="year" name="year" required>
                                <option value="" selected>YEAR</option>
                                <option value="1970">1950</option>
                                <option value="1971">1951</option>
                                <option value="1972">1952</option>
                                <option value="1973">1953</option>
                                <option value="1974">1954</option>
                                <option value="1975">1955</option>
                                <option value="1976">1956</option>
                                <option value="1977">1957</option>
                                <option value="1978">1958</option>
                                <option value="1979">1959</option>
                                <option value="1980">1960</option>
                                <option value="1981">1961</option>
                                <option value="1982">1962</option>
                                <option value="1983">1963</option>
                                <option value="1984">1964</option>
                                <option value="1985">1965</option>
                                <option value="1986">1966</option>
                                <option value="1987">1967</option>
                                <option value="1988">1968</option>
                                <option value="1989">1969</option>
                                <option value="1970">1970</option>
                                <option value="1971">1971</option>
                                <option value="1972">1972</option>
                                <option value="1973">1973</option>
                                <option value="1974">1974</option>
                                <option value="1975">1975</option>
                                <option value="1976">1976</option>
                                <option value="1977">1977</option>
                                <option value="1978">1978</option>
                                <option value="1979">1979</option>
                                <option value="1980">1980</option>
                                <option value="1981">1981</option>
                                <option value="1982">1982</option>
                                <option value="1983">1983</option>
                                <option value="1984">1984</option>
                                <option value="1985">1985</option>
                                <option value="1986">1986</option>
                                <option value="1987">1987</option>
                                <option value="1988">1988</option>
                                <option value="1989">1989</option>
                                <option value="1990">1990</option>
                                <option value="1991">1991</option>
                                <option value="1992">1992</option>
                                <option value="1993">1993</option>
                                <option value="1994">1994</option>
                                <option value="1995">1995</option>
                                <option value="1996">1996</option>
                                <option value="1997">1997</option>
                                <option value="1998">1998</option>
                                <option value="1999">1999</option>
                                <option value="2000">2000</option>
                                <option value="2001">2001</option>
                                <option value="2002">2002</option>
                                <option value="2003">2003</option>
                                <option value="2004">2004</option>
                                <option value="2005">2005</option>
                                <option value="2006">2006</option>
                                <option value="2007">2007</option>
                                <option value="2008">2008</option>
                                <option value="2009">2009</option>
                                <option value="2010">2010</option>
                                <option value="2011">2011</option>
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                            </select>
                        </div>
                    </td>
                    <td><input class="no-border" type="text" name="facebook" placeholder="FACEBOOK" id="facebookBecome" required></td>
                </tr>
            </table>

            <div class="upload">
                <div class="imageLoader">
                    <img id="thumbnail_image_1" src="media/postulation/upload1.png" alt="become image 1" style="width: 100%;">
                    <div class="overlay">
                        <div class="uploadLogo"><img src="media/postulation/upload-logo.png" /></div>
                    </div>
                    <input name="image_1" id="image_1" accept=".png,.jpg,.jpeg" type='file' class="uploadButton" onchange="loadImage(this,1)" required>
                </div>

                <div class="imageLoader">
                    <img id="thumbnail_image_2" src="media/postulation/upload2.png"  alt="become image 2" style="width: 100%;">
                    <div class="overlay">
                        <div class="uploadLogo"><img src="media/postulation/upload-logo.png" /></div>
                    </div>
                    <input name="image_2" id="image_2" accept=".png,.jpg,.jpeg" type='file' class="uploadButton" onchange="loadImage(this,2)" required/>
                </div>

                <div class="imageLoader">
                    <img id="thumbnail_image_3" src="media/postulation/upload3.png" alt="become image 3" style="width: 100%;">
                    <div class="overlay">
                        <div class="uploadLogo"><img src="media/postulation/upload-logo.png" /></div>
                    </div>
                    <input name="image_3" id="image_3" accept=".png,.jpg,.jpeg" type='file' class="uploadButton" onchange="loadImage(this,3)" required/>
                </div>

                <div class="imageLoader">
                    <img id="thumbnail_image_4" src="media/postulation/upload4.png" alt="become image 4" style="width: 100%;">
                    <div class="overlay">
                        <div class="uploadLogo "><img src="media/postulation/upload-logo.png" /></div>
                    </div>
                    <input name="image_4" id="image_4" accept=".png,.jpg,.jpeg" type='file' class="uploadButton" onchange="loadImage(this,4)" required/>
                </div>

            </div>

            <div class="buttonSubmit">
                <button  class="becomeSubmit" type="submit" >SUBMIT</button>
            </div>
        </form>
        
        </div>
    </section>
    <?php
}

function postulation_en() {
    entete("Le Casting Parisien, the best agency of Paris");
    nav_bar_2_en();
    postulation_section_en();
    pied("Follow Us");
}

//Fonctions pour la page de demande de devis
function ddv_section_en() {
    ?>
    <!--Formulaire-->
    <section class="module bg-dark-60 blog-page-header song" data-background="media/about_us/img.png">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3" style="width: 100%; margin-left: 0%;">
                    <h2 class="module-title font-alt" style="margin-top: 8%; margin-bottom: 11%;">Quote Request !</h2>
                    <p class="module-subtitle font-serif">Download our <a href="PRES.pdf" style="color: #ffffff;">presentation here</a>.</p>
              </div>
            </div>
        </div>
    </section>

    <section class="module">
        <div class="container">

        <?php
        if($_POST) {
            // if(mail($email_to, $subject, $message, $headers)) {
            if(true) {
                echo "<div class=\"alert alert-success\" role=\"alert\"><strong>Alerte !</strong> Votre demande a été envoyée !</div>";
            } else {
                echo "<div class=\"alert alert-danger\" role=\"alert\"><strong>Alerte !</strong> Votre demande n'a pas été envoyée !</div>";
            }
        }
        ?>

        <form id="Form" method="post" action="index_vf.php?page=ddv">
            <table style="margin-left: -8%;">
                <tr>
                    <td><input class="no-border" type="text" name="firstname" placeholder="FIRSTNAME" required></td>
                    <td><input class="no-border" type="text" name="surname" placeholder="SURNAME" required></td>
                    </tr>
                <tr>
                    <td><input class="no-border" type="text" name="email" placeholder="EMAIL" required></td>
                    <td><input class="no-border" type="text" name="name_entreprise" placeholder="COMPAGNY" required></td>
                </tr>
                <tr>
                    <td><input class="no-border" type="tel" name="phoneNumber" placeholder="PHONENUMBER" required></td>
                    <td><textarea class="no-border" name="demande" style="width: 100%" maxlength='2000' placeholder="YOUR INQUIRY (2.000)" required></textarea></td>
                </tr>
            </table>
                    
            <div class="buttonSubmit">
                <button  class="becomeSubmit" type="submit" >SUBMIT</button>
            </div>
        </form>
        </div>
    </section>
    <?php
}

function ddv_en() {
    entete("Le Casting Parisien, the best agency of Paris");
    nav_bar_2_en();
    ddv_section_en();
    pied("Follow Us");
}

?>